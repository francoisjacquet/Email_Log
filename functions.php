<?php
/**
 * Functions
 *
 * @package Email Log
 */

// Register functions to be hooked, last, if plugins before this one alters the email.
add_action( 'ProgramFunctions/SendEmail.fnc.php|before_send', 'EmailLogTriggered', 1, PHP_INT_MAX );

// Triggered function.
function EmailLogTriggered( $hook_tag )
{
	global $phpmailer,
		$_ROSARIO;

	static $email_array_tmp = [],
		$infinite_loop = false;

	if ( ! ( $phpmailer instanceof PHPMailer )
		&& ! ( $phpmailer instanceof PHPMailer\PHPMailer\PHPMailer ) ) // @since PHPMailer 6+
	{
		// Not sending email?
		return false;
	}

	if ( $infinite_loop )
	{
		// Infinite loop detected.
		// Infinite loop happens if error on email_log INSERT which triggers Email notification.
		return false;
	}

	$email_array = [
		'FROM_ADDRESS' => $phpmailer->From,
		'SUBJECT' => $phpmailer->Subject,
		'MESSAGE' => $phpmailer->Body,
		'CC' => $phpmailer->getCcAddresses(), // array.
		'BCC' => $phpmailer->getBccAddresses(), // array.
		'REPLY_TO' => $phpmailer->getReplyToAddresses(), // array.
		'ATTACHMENTS' => $phpmailer->getAttachments(), // array. Maybe /tmp uploaded files.
	];

	if ( empty( $email_array['REPLY_TO'] )
		&& ! Config( 'EMAIL_LOG_NOTIFICATIONS' ) )
	{
		// Email is a Notification: no "Reply To" addresses.
		return false;
	}

	// Format arrays to comma separated values:
	// to, reply-to, cc, bcc (only email addresses) & attachments (only file names).
	$extract_email_list = function( $emails )
	{
		$email_list = [];

		foreach ( (array) $emails as $email )
		{
			// Address is in [0], [1] is Name.
			$email_list[] = $email[0];
		}

		return implode( ',', $email_list );
	};

	if ( $email_array === $email_array_tmp
		&& ! empty( $_ROSARIO['Email_Log']['last_insert_id'] ) )
	{
		// Same email as before. UPDATE entry, append TO_ADDRESSES to 'TO_ADDRESSES' field only.
		DBQuery( "UPDATE email_log SET
			TO_ADDRESSES=CONCAT(TO_ADDRESSES, '," . DBEscapeString( $extract_email_list( $phpmailer->getToAddresses() ) ) . "')
			WHERE ID='" . (int) $_ROSARIO['Email_Log']['last_insert_id'] . "'" );

		return true;
	}

	$email_array_tmp = $email_array;

	$email = $email_array;

	$email['TO_ADDRESSES'] = $phpmailer->getToAddresses(); // array.

	$email['TO_ADDRESSES'] = $extract_email_list( $email['TO_ADDRESSES'] );

	$email['CC'] = $extract_email_list( $email['CC'] );

	$email['BCC'] = $extract_email_list( $email['BCC'] );

	$email['REPLY_TO'] = array_keys( (array) $email['REPLY_TO'] );

	$email['REPLY_TO'] = implode( ',', $email['REPLY_TO'] );

	// No Reply To address? Is a notification, do not save Staff ID.
	// No Staff ID? Is a student or not logged in, do not save Staff ID.
	$email['STAFF_ID'] = ! empty( $email['REPLY_TO'] ) && User( 'STAFF_ID' ) ? User( 'STAFF_ID' ) : '';
	$email['STUDENT_ID'] = ! empty( $email['REPLY_TO'] ) && ! User( 'STAFF_ID' ) && UserStudentID() ? UserStudentID() : '';

	$attachments_array = [];

	foreach ( (array) $email['ATTACHMENTS'] as $attachments )
	{
		// Path is in [0], [2] is Name.
		$attachments_array[] = $attachments[2];
	}

	$email['ATTACHMENTS'] = implode( ',', $attachments_array );

	array_rwalk( $email, 'DBEscapeString' );

	// Prevent infinite loop
	// Infinite loop happens if error on email_log INSERT which triggers Email notification.
	$infinite_loop = true;

	$_ROSARIO['Email_Log']['last_insert_id'] = DBInsert(
		'email_log',
		$email + [ 'SCHOOL_ID' => (int) UserSchool() ],
		'id'
	);

	$infinite_loop = false;

	return (bool) $_ROSARIO['Email_Log']['last_insert_id'];
}


// Register functions to be hooked.
add_action( 'ProgramFunctions/SendEmail.fnc.php|send_error', 'EmailLogErrorTriggered', 2 );

// Triggered function.
function EmailLogErrorTriggered( $hook_tag, $error )
{
	global $phpmailer,
		$_ROSARIO;

	if ( ! ( $phpmailer instanceof PHPMailer )
		&& ! ( $phpmailer instanceof PHPMailer\PHPMailer\PHPMailer ) ) // @since PHPMailer 6+
	{
		// Not sending email?
		return false;
	}

	if ( empty( $_ROSARIO['Email_Log']['last_insert_id'] )
		|| ! $error )
	{
		return false;
	}

	// UPDATE Email based on message ID (set UPDATED_AT).
	return DBUpdate(
		'email_log',
		[ 'ERROR' => DBEscapeString( strip_tags( $error ) ) ],
		[ 'ID' => (int) $_ROSARIO['Email_Log']['last_insert_id'] ]
	);
}
