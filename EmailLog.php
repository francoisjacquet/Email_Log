<?php
/**
 * Email Log program
 *
 * @package Email Log
 */

DrawHeader( ProgramTitle() );

// Set start date.
$start_date = RequestedDate( 'start', date( 'Y-m-d', time() - 60 * 60 * 24 ) );

// Set end date.
$end_date = RequestedDate( 'end', DBDate() );

if ( $_REQUEST['modfunc'] === 'update'
	&& AllowEdit() )
{
	if ( ! empty( $_REQUEST['config'] ) )
	{
		foreach ( (array) $_REQUEST['config'] as $column => $value )
		{
			Config(
				$column,
				$value
			);
		}
	}

	RedirectURL( [ 'modfunc', 'config' ] );
}

/*if ( $_REQUEST['modfunc'] === 'delete'
	&& AllowEdit() )
{
	// Prompt before deleting log.
	if ( DeletePrompt( dgettext( 'Email_Log', 'Email Log' ) ) )
	{
		DBQuery( 'DELETE FROM email_log' );

		$note[] = dgettext( 'Email_Log', 'Email Log cleared.' );

		// Unset modfunc & redirect URL.
		RedirectURL( 'modfunc' );
	}
}*/

if ( count( $_REQUEST ) === 3
	&& ! $_REQUEST['modfunc']
	&& Config( 'EMAIL_LOG_DELETE_OLDER_THAN_DAYS' ) )
{
	// Only requested modname.
	$days_ago_date = date(
		'Y-m-d',
		strtotime( ( (int) Config( 'EMAIL_LOG_DELETE_OLDER_THAN_DAYS' ) ) . ' days ago' )
	);

	// Automatically delete entries older than X days
	DBQuery( "DELETE FROM email_log
		WHERE CREATED_AT<'" . $days_ago_date . "';" );
}

echo ErrorMessage( $note, 'note' );

if ( ! $_REQUEST['modfunc']
	&& isset( $_REQUEST['tab'] )
	&& $_REQUEST['tab'] === 'config' )
{
	echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&tab=config&modfunc=update' ) . '" method="POST">';

	DrawHeader(
		'<a href="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] ) . '">« ' . _( 'Back' ) . '</a>',
		SubmitButton()
	);

	echo '<br>';

	PopTable( 'header', _( 'Configuration' ) );

	$delete_input = '<table class="cellspacing-0"><tr><td>' . sprintf(
		dgettext( 'Email_Log', 'Automatically delete entries older than %s days' ),
		'</td><td>' . TextInput(
			Config( 'EMAIL_LOG_DELETE_OLDER_THAN_DAYS' ),
			'config[EMAIL_LOG_DELETE_OLDER_THAN_DAYS]',
			'',
			'type="number" min="0" max="9999"'
		) . '</td><td>'
	) . '</td></tr></table>';

	$tooltip = '<div class="tooltip"><i>' . dgettext(
		'Email_Log',
		'Notifications are emails automatically sent by RosarioSIS, without a "Reply To" address.'
	) . '</i></div>';

	$log_notifications_checkbox = CheckboxInput(
		Config( 'EMAIL_LOG_NOTIFICATIONS' ),
		'config[EMAIL_LOG_NOTIFICATIONS]',
		dgettext( 'Email_Log', 'Log Notifications' ) . $tooltip,
		'',
		false,
		button( 'check' ),
		button( 'x' )
	);

	echo $delete_input . '<br>' .
		$log_notifications_checkbox;

	PopTable( 'footer' );

	echo '<br><div class="center">' . SubmitButton() . '</div></form>';
}

if ( ! $_REQUEST['modfunc']
	&& empty( $_REQUEST['tab'] ) )
{
	echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname']  ) . '" method="GET">';

	if ( ! AllowEdit() )
	{
		$_ROSARIO['allow_edit'] = true;

		$allow_edit_tmp = true;
	}

	DrawHeader(
		_( 'From' ) . ' ' . DateInput( $start_date, 'start', '', false, false ) . ' - ' .
		_( 'To' ) . ' ' . DateInput( $end_date, 'end', '', false, false ) .
		Buttons( _( 'Go' ) ),
		'<a href="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&tab=config' ) . '">' . _( 'Configuration' ) . '</a>'
	);

	if ( ! empty( $allow_edit_tmp ) )
	{
		$_ROSARIO['allow_edit'] = false;
	}

	echo '</form>';

	// Format DB data.
	$email_logs_functions = [
		'CREATED_AT' => 'ProperDateTime',
		'FULL_NAME' => '_makeEmailLogFullName',
		'TO_ADDRESSES' => '_makeEmailLogTruncate',
		'SUBJECT' => '_makeEmailLogTruncate',
		'MESSAGE' => '_makeEmailLogMessage',
		'ERROR' => '_makeEmailLogTruncate',
	];

	$email_logs_sql = "SELECT ID,el.CREATED_AT,FROM_ADDRESS,TO_ADDRESSES,SUBJECT,MESSAGE,CC,BCC,REPLY_TO,ATTACHMENTS,ERROR,
		el.STUDENT_ID,el.STAFF_ID,
		" . DisplayNameSQL( 's' ) . " AS FULL_NAME,s.SYEAR AS STAFF_SYEAR,s.PROFILE,
		" . DisplayNameSQL( 'st' ) . " AS FULL_NAME_STUDENT
		FROM email_log el
		LEFT JOIN staff s ON (s.STAFF_ID=el.STAFF_ID)
		LEFT JOIN students st ON (st.STUDENT_ID=el.STUDENT_ID)
		WHERE (el.SCHOOL_ID='" . UserSchool() . "' OR el.SCHOOL_ID='0')
		AND el.CREATED_AT>='" . $start_date . "'
		AND el.CREATED_AT<='" . $end_date . ' 23:59:59' . "'
		ORDER BY el.CREATED_AT DESC";

	if ( function_exists( 'SQLLimitForList' ) )
	{
		// @since RosarioSIS 11.7 Limit SQL result for List
		$sql_count = "SELECT COUNT(1)
			FROM email_log el
			LEFT JOIN staff s ON (s.STAFF_ID=el.STAFF_ID)
			LEFT JOIN students st ON (st.STUDENT_ID=el.STUDENT_ID)
			WHERE (el.SCHOOL_ID='" . UserSchool() . "' OR el.SCHOOL_ID='0')
			AND el.CREATED_AT>='" . $start_date . "'
			AND el.CREATED_AT<='" . $end_date . ' 23:59:59' . "'";

		$email_logs_sql .= SQLLimitForList( $sql_count );
	}
	else
	{
		$email_logs_sql .= " LIMIT 3000";
	}

	$email_logs_RET = DBGet( $email_logs_sql, $email_logs_functions );

	/*echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&modfunc=delete' ) . '" method="POST">';

	DrawHeader( '', SubmitButton( _( 'Clear Log' ), '', '' ) );

	echo '</form>';*/

	ListOutput(
		$email_logs_RET,
		[
			'CREATED_AT' => _( 'Date' ),
			'FULL_NAME' => dgettext( 'Email_Log', 'Sent by' ),
			'TO_ADDRESSES' => dgettext( 'Email_Log', 'To' ),
			'SUBJECT' => dgettext( 'Email_Log', 'Subject' ),
			'MESSAGE' => _( 'Message' ),
			'ERROR' => _( 'Error' ),
		],
		dgettext( 'Email_Log', 'Email' ),
		dgettext( 'Email_Log', 'Emails' ),
		[],
		[],
		[
			'count' => true,
			'save' => true,
			// Add pagination for list > 1000 results
			'pagination' => true,
			'valign-middle' => true,
		]
	);

}


/**
 * Make Message ColorBox
 *
 * Local function
 * DBGet callback
 *
 * @param  string $value   Field value.
 * @param  string $name    'MESSAGE'.
 *
 * @return string
 */
function _makeEmailLogMessage( $value, $column )
{
	global $THIS_RET;

	$message = $value;

	if ( strip_tags( $message ) === $message )
	{
		// Message is text.
		$message = '<p style="white-space: pre-wrap;">' . $message . '</p>';
	}

	if ( isset( $_REQUEST['LO_save'] ) )
	{
		return $message;
	}

	ob_start();

	DrawHeader(
		'<b>' . dgettext( 'Email_Log', 'Subject' ) . ':</b> ' . $THIS_RET['SUBJECT']
	);

	DrawHeader(
		'<b>' . dgettext( 'Email_Log', 'From' ) . ':</b> ' . $THIS_RET['FROM_ADDRESS']
	);

	DrawHeader(
		'<b>' . dgettext( 'Email_Log', 'To' ) . ':</b> ' . $THIS_RET['TO_ADDRESSES']
	);

	DrawHeader(
		'<b>' . _( 'Date' ) . ':</b> ' . ProperDateTime( $THIS_RET['CREATED_AT'] )
	);

	if ( $THIS_RET['CC'] )
	{
		DrawHeader(
			'<b>' . dgettext( 'Email_Log', 'Carbon Copy' ) . ':</b> ' . $THIS_RET['CC']
		);
	}

	if ( $THIS_RET['BCC'] )
	{
		DrawHeader(
			'<b>' . dgettext( 'Email_Log', 'Blind Carbon Copy' ) . ':</b> ' . $THIS_RET['BCC']
		);
	}

	if ( $THIS_RET['REPLY_TO'] )
	{
		DrawHeader(
			'<b>' . dgettext( 'Email_Log', 'Reply To' ) . ':</b> ' . $THIS_RET['REPLY_TO']
		);
	}

	if ( $THIS_RET['ATTACHMENTS'] )
	{
		DrawHeader(
			'<b>' . _( 'File Attached' ) . ':</b> ' . $THIS_RET['ATTACHMENTS']
		);
	}

	if ( $THIS_RET['ERROR'] )
	{
		DrawHeader(
			'<b>' . _( 'Error' ) . ':</b> ' . $THIS_RET['ERROR']
		);
	}

	$headers = ob_get_clean();

	$content = $headers . $message;

	$return = '<div style="display:none;"><div id="EmailLogMsg_' . $THIS_RET['ID'] . '">' .
		$content . '</div></div>';

	$return .= '<a class="colorboxinline" href="#EmailLogMsg_' . $THIS_RET['ID'] . '">' .
		button( 'visualize', _( 'View Online' ), '', 'bigger' ) . '</a>';

	return $return;
}

function _makeEmailLogTruncate( $value, $column = 'TO_ADDRESSES' )
{
	if ( isset( $_REQUEST['_ROSARIO_PDF'] )
		|| ! $value )
	{
		return $value;
	}

	// Truncate value to 40 chars.
	$val = mb_strlen( $value ) <= 40 ?
		$value :
		'<span title="' . AttrEscape( $value ) . '">' . mb_substr( $value, 0, 37 ) . '...</span>';

	return $val;
}



/**
 * Make Full Name
 * Add link to User Info/Student Info
 *
 * Local function
 * DBGet callback
 *
 * @param  string $value   Field value.
 * @param  string $name    'FULL_NAME'.
 *
 * @return string
 */
function _makeEmailLogFullName( $value, $column )
{
	global $THIS_RET;

	if ( $value )
	{
		$profile = [
			'admin' => _( 'Administrator' ),
			'teacher' => _( 'Teacher' ),
			'parent' => _( 'Parent' ),
		];

		if ( ! AllowUse( 'Users/User.php' )
			|| empty( $THIS_RET['STAFF_ID'] )
			|| $THIS_RET['STAFF_SYEAR'] !== UserSyear() ) // User ID is not in current SYEAR...
		{
			if ( ! empty( $THIS_RET['PROFILE'] ) )
			{
				return '<span title="' . AttrEscape( $profile[ $THIS_RET['PROFILE'] ] ) . '">' .
					$value . '</span>';
			}

			return $value;
		}

		$url = 'Modules.php?modname=Users/User.php&staff_id=' . $THIS_RET['STAFF_ID'];

		return '<a href="' . URLEscape( $url ) . '" title="' .
			AttrEscape( $profile[ $THIS_RET['PROFILE'] ] ) . '">' . $value . '</a>';
	}

	if ( ! empty( $THIS_RET['FULL_NAME_STUDENT'] ) )
	{
		if ( ! AllowUse( 'Students/Student.php' )
			|| empty( $THIS_RET['STUDENT_ID'] ) ) // Should check if student enrolled in current SYEAR...
		{
			return '<span title="' . AttrEscape( _( 'Student' ) ) . '">' .
				$THIS_RET['FULL_NAME_STUDENT'] . '</span>';
		}

		$url = 'Modules.php?modname=Students/Student.php&student_id=' . $THIS_RET['STUDENT_ID'];

		return '<a href="' . URLEscape( $url ) . '" title="' .
			AttrEscape( _( 'Student' ) ) . '">' . $THIS_RET['FULL_NAME_STUDENT'] . '</a>';
	}

	return '';
}
