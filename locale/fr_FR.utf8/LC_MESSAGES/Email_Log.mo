��          �      �       H  /   I     y     �     �  	   �     �     �     �  X   �          '     /     7  �  :  A   +     m     {     �     �     �     �     �  g   �     *     7     C     I           
       	                                               Automatically delete entries older than %s days Blind Carbon Copy Carbon Copy Email Email Log Emails From Log Notifications Notifications are emails automatically sent by RosarioSIS, without a "Reply To" address. Reply To Sent by Subject To Project-Id-Version: Email Log module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-01-09 18:17+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2;ndgettext:2,3
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 Supprimer automatiquement les entrées datant de plus de %s jours Copie cachée Copie Email Journal des emails Emails De Enregistrer les notifications Les notifications sont les emails envoyés automatiquement par RosarioSIS, sans adresse "Répondre à". Répondre à Envoyé par Sujet À 