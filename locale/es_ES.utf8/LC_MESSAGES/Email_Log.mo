��          �      �       H  /   I     y     �     �  	   �     �     �     �  X   �          '     /     7    :  :   I     �     �     �     �     �     �     �  e   �     <     H     T     [           
       	                                               Automatically delete entries older than %s days Blind Carbon Copy Carbon Copy Email Email Log Emails From Log Notifications Notifications are emails automatically sent by RosarioSIS, without a "Reply To" address. Reply To Sent by Subject To Project-Id-Version: Email Log module for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-01-09 18:13+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2;ngettext:2,3
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Eliminar automáticamente las entradas de más de %s días Copia oculta Copia Email Registro de Email Emails De Registrar las notificaciones Las notificaciones son emails enviados automáticamente por RosarioSIS, sin dirección "Responder a". Responder a Enviado por Asunto A 