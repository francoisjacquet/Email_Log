/**
 * Install MySQL
 * Required if the module has menu entries
 * - Add profile exceptions for the module to appear in the menu
 * - Add program config options if any (to every schools)
 * - Add module specific tables (and their eventual sequences & indexes)
 *   if any: see rosariosis.sql file for examples
 *
 * @package Email Log module
 */

/**
 * profile_exceptions Table
 *
 * profile_id:
 * - 0: student
 * - 1: admin
 * - 2: teacher
 * - 3: parent
 * modname: should match the Menu.php entries
 * can_use: 'Y'
 * can_edit: 'Y' or null (generally null for non admins)
 */
--
-- Data for Name: profile_exceptions; Type: TABLE DATA;
--

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Email_Log/EmailLog.php', 'Y', 'Y'
FROM DUAL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Email_Log/EmailLog.php'
    AND profile_id=1);


/**
 * config Table
 *
 * syear: school year (school may have various years in DB)
 * school_id: may exists various schools in DB
 * program: convention is plugin name, for ex.: 'ldap'
 * title: for ex.: 'EMAIL_LOG_[your_config]'
 * value: string
 */
--
-- Data for Name: config; Type: TABLE DATA; Schema: public; Owner: rosariosis
--

INSERT INTO config (school_id, title, config_value)
SELECT 0, 'EMAIL_LOG_NOTIFICATIONS', NULL
FROM DUAL
WHERE NOT EXISTS (SELECT title
    FROM config
    WHERE title='EMAIL_LOG_NOTIFICATIONS');

INSERT INTO config (school_id, title, config_value)
SELECT 0, 'EMAIL_LOG_DELETE_OLDER_THAN_DAYS', '365'
FROM DUAL
WHERE NOT EXISTS (SELECT title
    FROM config
    WHERE title='EMAIL_LOG_DELETE_OLDER_THAN_DAYS');



/**
 * Add module tables
 */

/**
 * Email Log table
 */
--
-- Name: email_log; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE TABLE IF NOT EXISTS email_log (
    id integer NOT NULL AUTO_INCREMENT PRIMARY KEY,
    school_id integer NOT NULL,
    staff_id integer, -- Can be NULL for Notifications
    FOREIGN KEY (staff_id) REFERENCES staff(staff_id),
    student_id integer, -- Can be NULL
    FOREIGN KEY (student_id) REFERENCES students(student_id),
    from_address text NOT NULL,
    to_addresses longtext NOT NULL,
    subject text NOT NULL,
    message longtext,
    cc longtext,
    bcc longtext,
    reply_to text,
    attachments text,
    error text,
    created_at timestamp DEFAULT current_timestamp,
    updated_at timestamp NULL ON UPDATE current_timestamp
);
