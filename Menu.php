<?php
/**
 * Menu.php file
 * Required
 * - Menu entries for the Email Log module
 * - Add Menu entries to other modules
 *
 * @package Email Log module
 */

/**
 * Use dgettext() function instead of _() for Module specific strings translation
 * see locale/README file for more information.
 */
$module_name = dgettext( 'Email_Log', 'Email Log' );

// Menu entries for the School Setup module.
$menu['School_Setup']['admin']['Email_Log/EmailLog.php'] = dgettext( 'Email_Log', 'Email Log' );
