Email Log module
================

![screenshot](https://gitlab.com/francoisjacquet/Email_Log/raw/main/screenshot.png?inline=false)

https://gitlab.com/francoisjacquet/Email_Log

Version 1.4 - May, 2024

License GNU/GPLv2 or later

Author François Jacquet

Sponsored by AT Group, Slovenia

DESCRIPTION
-----------
Email Log module for RosarioSIS. This module saves every email sent from RosarioSIS. You can later consult emails from the _School > Email Log_ program.

Can be used for audit or debug purpose.

Note: the list displays emails for the current school only, so it can be used safely in a multi-school environment.

Configuration:
- Log Notifications: Notifications are emails automatically sent by RosarioSIS, without a "Reply To" address.
- Automatically delete entries older than X days

Translated in [French](https://www.rosariosis.org/fr/modules/email-log/) & [Spanish](https://www.rosariosis.org/es/modules/email-log/).

CONTENT
-------
School
- Email Log

INSTALL
-------
Copy the `Email_Log/` folder (if named `Email_Log-main`, rename it) and its content inside the `modules/` folder of RosarioSIS.

Go to _School > Configuration > Modules_ and click "Activate".

Requires RosarioSIS 11.0+
