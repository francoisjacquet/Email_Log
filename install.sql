/**
 * Install PostgreSQL
 * Required if the module has menu entries
 * - Add profile exceptions for the module to appear in the menu
 * - Add program config options if any (to every schools)
 * - Add module specific tables (and their eventual sequences & indexes)
 *   if any: see rosariosis.sql file for examples
 *
 * @package Email Log module
 */

-- Fix #102 error language "plpgsql" does not exist
-- http://timmurphy.org/2011/08/27/create-language-if-it-doesnt-exist-in-postgresql/
--
-- Name: create_language_plpgsql(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION create_language_plpgsql()
RETURNS BOOLEAN AS $$
    CREATE LANGUAGE plpgsql;
    SELECT TRUE;
$$ LANGUAGE SQL;

SELECT CASE WHEN NOT (
    SELECT TRUE AS exists FROM pg_language
    WHERE lanname='plpgsql'
    UNION
    SELECT FALSE AS exists
    ORDER BY exists DESC
    LIMIT 1
) THEN
    create_language_plpgsql()
ELSE
    FALSE
END AS plpgsql_created;

DROP FUNCTION create_language_plpgsql();



/**
 * profile_exceptions Table
 *
 * profile_id:
 * - 0: student
 * - 1: admin
 * - 2: teacher
 * - 3: parent
 * modname: should match the Menu.php entries
 * can_use: 'Y'
 * can_edit: 'Y' or null (generally null for non admins)
 */
--
-- Data for Name: profile_exceptions; Type: TABLE DATA;
--

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Email_Log/EmailLog.php', 'Y', 'Y'
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Email_Log/EmailLog.php'
    AND profile_id=1);


/**
 * config Table
 *
 * syear: school year (school may have various years in DB)
 * school_id: may exists various schools in DB
 * program: convention is plugin name, for ex.: 'ldap'
 * title: for ex.: 'EMAIL_LOG_[your_config]'
 * value: string
 */
--
-- Data for Name: config; Type: TABLE DATA; Schema: public; Owner: rosariosis
--

INSERT INTO config (school_id, title, config_value)
SELECT 0, 'EMAIL_LOG_NOTIFICATIONS', NULL
WHERE NOT EXISTS (SELECT title
    FROM config
    WHERE title='EMAIL_LOG_NOTIFICATIONS');

INSERT INTO config (school_id, title, config_value)
SELECT 0, 'EMAIL_LOG_DELETE_OLDER_THAN_DAYS', '365'
WHERE NOT EXISTS (SELECT title
    FROM config
    WHERE title='EMAIL_LOG_DELETE_OLDER_THAN_DAYS');



/**
 * Add module tables
 */


/**
 * Email Log table
 */
--
-- Name: email_log; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE OR REPLACE FUNCTION create_table_email_log() RETURNS void AS
$func$
BEGIN
    IF EXISTS (SELECT 1 FROM pg_catalog.pg_tables
        WHERE schemaname=CURRENT_SCHEMA()
        AND tablename='email_log') THEN
    RAISE NOTICE 'Table "email_log" already exists.';
    ELSE
        CREATE TABLE email_log (
            id serial PRIMARY KEY,
            school_id integer NOT NULL,
            staff_id integer REFERENCES staff(staff_id), -- Can be NULL for Notifications
            student_id integer REFERENCES students(student_id), -- Can be NULL
            from_address text NOT NULL,
            to_addresses text NOT NULL,
            subject text NOT NULL,
            message text,
            cc text,
            bcc text,
            reply_to text,
            attachments text,
            error text,
            created_at timestamp DEFAULT current_timestamp,
            updated_at timestamp
        );

        CREATE TRIGGER set_updated_at
            BEFORE UPDATE ON email_log
            FOR EACH ROW EXECUTE PROCEDURE set_updated_at();
    END IF;
END
$func$ LANGUAGE plpgsql;

SELECT create_table_email_log();
DROP FUNCTION create_table_email_log();
